from application import app, db, sess
from flask_script import Manager, prompt_bool, Server
from application.models import User, Keyword, Category, News, About, Website, Contact, Syllabus, Course, Term

manager = Manager(app)

# uncomment bellow lines when run in debug mode
manager.add_command('runserver', Server(port=5000, host='127.0.0.1'))
manager.add_command('debug', Server(use_debugger=True, use_reloader=True, port=5000, host='127.0.0.1'))


@manager.command
def initdb():
    """ Initial database """
    db.create_all()
    sess.app.session_interface.db.create_all()
    print('Create db shema successfully')
    # if prompt_bool("Do you want initial db with data?"):
    #     init_data()
    #     print('Init db successfully')


@manager.command
def init():
    """ Initial database """
    term = Term(semester=Term.SEMESTER_ONE, year=1396)
    db.session.add(term)
    db.session.commit()

    course = Course(title='مهندسی اینترنت', term_id=term.id)
    db.session.add(course)
    db.session.commit()

    admin = User('مهدی', 'شریفی', 'a', 'a', User.ROLE_ADMIN)
    teacher = User('رضا', 'معانی جو', 't', 't', User.ROLE_PROFESSOR)
    student = User('محمد', 'مهدی', 's', 's', User.ROLE_STUDENT)
    db.session.add(admin)
    db.session.add(teacher)
    db.session.add(student)
    db.session.commit()

    course.users.append(teacher)
    course.users.append(student)

    db.session.commit()

    print('init successfully')


@manager.command
def dropdb():
    """ Drop database """
    if prompt_bool("Are you sure you want to lose all your data?"):
        db.drop_all()
        print('Drop db successfully')
    else:
        print('Drop db aborted')


# def init_data():
#     website1 = Website('github')
#     website2 = Website('gmail')
#
#     user = User('مهدی', 'شریفی', 'winkers', '123456')
#     contact1 = Contact(user.id, website1.id, 'github.com/winkersco')
#     contact2 = Contact(user.id, website2.id, 'mehdi.sh4rifi96@gmail.com')
#     website1.contacts.append(contact1)
#     website2.contacts.append(contact2)
#     user.contacts.append(contact1)
#     user.contacts.append(contact2)
#     db.session.add(user)
#     db.session.commit()
#
#     professor1 = User('رضا', 'معانی جو', 'maanijou', 'maanijou', User.ROLE_PROFESSOR)
#     contact1 = Contact(professor1.id, website2.id, 'reza.maanijou@gmail.com')
#     website2.contacts.append(contact1)
#     professor1.contacts.append(contact1)
#     db.session.add(contact1)
#     db.session.commit()
#
#     professor2 = User('مجتبی', 'شاکری', 'shakeri', 'shakeri', User.ROLE_PROFESSOR)
#     db.session.add(professor2)
#     db.session.commit()
#
#     professor3 = User('ابولقاسم', 'میرروشندل', 'mirroshandel', 'mirroshandel', User.ROLE_PROFESSOR)
#     db.session.add(professor3)
#     db.session.commit()
#
#     course1 = Course('مهندسی اینترنت',
#                      'هدف این درس، ارائه دانش و مهارت‌های پایه برای برنامه‌نویسی مبتنی بر وب و اینترنت است. هر چند '
#                      'سرعت تغییر فناوری‌های این حوزه به سرعت زیاد است، اما در این درس تلاش می‌شود اصول برنامه‌نویسی و '
#                      'طراحی معماری چنین نرم‌افزارهایی مورد بحث قرار بگیرد. معماری مورد بحث در این درس شامل یک بخش '
#                      'back-end ​مشتمل​​​​ بر لایه‌های منطق دامنه و دسترسی به داده‌هاست که با زبان جاوا تولید شده است و '
#                      'خدمات مورد نظر را به لایه‌ی front-end که به سبک کاربردهای اینترنتی غنی (RIA) به زبان جاواسکریپت '
#                      'تولید شده ارائه می‌کند. بخش اول درس، ​اختصاص دارد به برنامه‌نویسی شبکه و وب در جاوا که بعد از '
#                      'مرور کوتاهی روی مفاهیم پایه وب به بیان روش تولید برنامه‌های مبتنی بر سرولت و جی‌اس‌پی می‌پردازد. '
#                      'در بخش دوم، تولید برنامه‌های غنی طرف کلاینت با جاواسکریپت مورد بررسی قرار می‌گیر​د​.​ بخش سوم '
#                      '​درس ​به بیان الگوهای معماری تولید نرم‌افزارهای سازمانی از قبیل نگارنده‌های اشیاء به روابط، '
#                      'مدیریت نشست‌ها، کنترل همروندی و توزیع‌شدگی می‌پردازد پیش‌نیاز گذراندن این درس، ​دانستن​ '
#                      'برنامه‌نویسی شیءگرا به یکی از زبان‌های سی‌پلاس‌پلاس، جاوا یا سی‌شارپ است. البته آشنایی کلی با '
#                      'مفاهیم پایه شبکه و پایگاه داده به فهم بهتر مطلب کمک می‌کند.')
#     course2 = Course('طراحی الگوریتم', 'درس طراحی و تحلیل الگوریتم‌ها یکی از پایه‌ای‌ترین درس‌های در رشته‌های علوم '
#                                        'کامپیوتر و همچنین مهندسی کامپیوتر می‌باشد. هدف از این درس، مطالعه و بررسی '
#                                        'روش‌های طراحی الگوریتم‌ها برای حل مسائل مختلف و چگونگی تحلیل و اثبات درستی '
#                                        'آنها می‌باشد. همچنین دسته‌بندی مسائل و شناسایی مسائل محاسباتی سخت، که در زمان '
#                                        'قابل قبول نمی‌توان جواب آنها را به دست آورد، نیز پوشش داده می‌شود. ')
#     course3 = Course('هوش مصنوعی',
#                      'در اين درس جنبه هایی از هوشمندی نظير حل مساله، توانایی ذخيره دانش و استنتاج و همچنين برنامه '
#                      'ريزی مورد بررسی قرار خواهد گرفت. از آنجاکه دسته گسترده ای از مسائل می توانند در قالب مساله '
#                      'جستجو بيان شوند، ابتدا در مورد استفاده از روشهای جستجو برای حل مسائل بحث خواهد شد. همچنين '
#                      'چگونگی استفاده از دانش خاص يک مساله (يا دسته ای از مسائل) جهت بهبود زمان جستجو بررسی می شود. '
#                      'ذخيره دانش و استنتاج نيز بخش ديگری از توانایی عاملهای هوشمند است که در اين درس به آن پرداخته '
#                      'خواهد شد. در اين خصوص به منطق گزاره ای، مرتبه اول و منطق احتمالاتی و چگونگی استنتاج در اين منطق '
#                      'ها می پردازيم. همچنين درباره مبحث برنامه ريزی که هدف آن يافتن برنامه ای از کنشها برای رسيدن به '
#                      'اهداف است، صحبت خواهد شد. ')
#     db.session.add(course1)
#     db.session.add(course2)
#     db.session.add(course3)
#     db.session.commit()
#
#     syllabus1 = Syllabus(course1.id, 'مفاهیم پایه و مبانی شبکه های کامپیوتری')
#     syllabus2 = Syllabus(course1.id, 'برنامه نویسی سوکت')
#     syllabus3 = Syllabus(course1.id, 'امنیت اطلاعات در شبکه')
#     syllabus4 = Syllabus(course2.id, 'تحلیل الگوریتم')
#     syllabus5 = Syllabus(course2.id, 'الگوریتم های حریصانه')
#     syllabus6 = Syllabus(course2.id, 'برنامه ریزی پویا')
#     syllabus7 = Syllabus(course3.id, 'روش های جستجو')
#     syllabus8 = Syllabus(course3.id, 'جستجوی نااگاهانه')
#     syllabus9 = Syllabus(course3.id, 'جستجو آگاهانه')
#     syllabus10 = Syllabus(course3.id, 'روش های یادگیری')
#     db.session.add(syllabus1)
#     db.session.add(syllabus2)
#     db.session.add(syllabus3)
#     db.session.add(syllabus4)
#     db.session.add(syllabus5)
#     db.session.add(syllabus6)
#     db.session.add(syllabus7)
#     db.session.add(syllabus8)
#     db.session.add(syllabus9)
#     db.session.add(syllabus10)
#     term1 = Term(1396, Term.SEMESTER_ONE)
#     term2 = Term(1396, Term.SEMESTER_TWO)
#     term3 = Term(1396, Term.SEMESTER_SUMMER)
#     db.session.add(term1)
#     db.session.add(term2)
#     db.session.add(term3)
#     db.session.commit()
#
#     user.courses.append(course1)
#     user.courses.append(course2)
#     user.courses.append(course3)
#     professor1.courses.append(course1)
#     professor2.courses.append(course2)
#     professor3.courses.append(course3)
#
#     term1.courses.append(course1)
#     term2.courses.append(course2)
#     term2.courses.append(course3)
#     term3.courses.append(course3)
#
#     db.session.commit()
#
#     keyword = Keyword('امتحانات')
#     category = Category('امور پژوهشی')
#     db.session.add(keyword)
#     db.session.add(category)
#
#     news = News(db.session.query(User.id).first().id, 'کلاس فوق العاده درس مبانی کامپیوتر و برنامه سازی',
#               'کلاس فوق العاده درس مبانی کامپیوتر و'
#               ' برنامه سازی برای هر دو گروه دانشجویان روز سه شنبه 07/09/1396  ساعت 15-17 در آمفی تئاتر'
#               ' شهید نورانی دانشکده فنی برگزار می گردد.')
#     category = Category('برگزاری کلاس')
#     news.categories.append(category)
#     db.session.add(news)
#
#     news = News(db.session.query(User.id).first().id, 'اطلاعیه مهم در مورد کارآموزی',
#               'ا توجه به مهلت ثبت نمره درس کارآموزی ت'
#               ' 15 آبان 1396، دانشجویانی که درس '
#               'کارآموزی را در نیمسال دوم 1395 و هم چنین'
#               ' تابستان 96 اخذ نموده اند،هرچه سریع تر نسبت به تایید'
#               ' گزارش کارآموزی با استاد مربوطه اقدام نمایند و'
#               ' تمامی فرم ها را در قسمت گردش اتمام '
#               'کارآموزی در کارتابل دانشجویی خود بارگذاری نمایند.')
#     category = Category('کارآموزی')
#     keyword1 = Keyword('اضطراری')
#     keyword2 = Keyword('کزارش')
#     news.categories.append(category)
#     news.keywords.append(keyword1)
#     news.keywords.append(keyword2)
#     db.session.add(news)
#
#     about = About('''
#     گروه مهندسی کامپیوتر دانشگاه گیلان از مهرماه 1386 با پذیرش 40 دانشجو در یک رشته کارشناسی مهندسی کامپیوتر
#     گرایش نرم افزار دوره روزانه و با 3 هیأت علمی و یک کارشناس فعالیت خود را آغاز نمود
#     و در حال حاضر دارای رشته های کارشناسی مهندسی کامپیوتر گرایشهای نرم افزار، سخت افزار و کارشناسی ارشد
#     مهندسی نرم افزار و کارشناسی ارشد معماری سیستم های کامپیوتری میباشد. در کارشناسی گرایش نرم افزار دارای
#      197 دانشجو، در گرایش سخت افزار 100 دانشجو و در رشته ارشد نرم افزار 32 دانشجو
#       و در کارشناسی ارشد معماری سیستم های کامپیوتری 16 دانشجو در حال تحصیل هستند.
#     ''')
#     db.session.add(about)
#
#     db.session.commit()


if __name__ == '__main__':
    manager.run()
