from flask_wtf import FlaskForm
from flask_wtf.file import FileRequired, FileAllowed
from wtforms.widgets import HiddenInput

from application.models import Term, User
from wtforms import IntegerField, SelectField, StringField, PasswordField, SelectMultipleField, TextAreaField, \
    FileField
from wtforms.fields.html5 import DateField
from wtforms.validators import InputRequired


class Unique(object):
    """ validator that checks field uniqueness """

    def __init__(self, model, field, message=None):
        self.model = model
        self.field = field
        if not message:
            message = u'this element already exists'
        self.message = message

    def __call__(self, form, field):
        check = self.model.query.filter(self.field == field.data).first()
        if 'id' in form:
            id = form.id.data
        else:
            id = None
        if check and (id is None or id != check.id):
            raise ValueError(self.message)


class TermForm(FlaskForm):
    semester = SelectField('ترم', choices=Term.get_semester_list(),
                           validators=[InputRequired(message='ترم نمیتواند خالی باشد')])
    year = IntegerField('سال', validators=[InputRequired(message='سال نمیتواند خالی باشد')])


class CourseForm(FlaskForm):
    term = SelectField('ترم', validators=[InputRequired(message='ترم نمیتواند خالی باشد')])
    title = StringField('عنوان', validators=[InputRequired(message='عنوان نمیتواند خالی باشد')])


class ProfessorForm(FlaskForm):
    id = IntegerField(widget=HiddenInput())
    firstname = StringField('نام', validators=[InputRequired(message='نام نمیتواند خالی باشد')])
    lastname = StringField('نام خانوادگی', validators=[InputRequired(message='نام خانوادگی نمیتواند خالی باشد')])
    username = StringField('نام کاربری', validators=[InputRequired(message='نام کاربری نمیتواند خالی باشد'),
                                                     Unique(User, User.username, 'این نام کاربری قبلا ثبت شده است')])
    password = PasswordField('رمز عبور', validators=[InputRequired(message='رمز عبور نمیتواند خالی باشد')])


class UserUpdateForm(FlaskForm):
    password = PasswordField('رمز عبور', validators=[InputRequired(message='رمز عبور نمیتواند خالی باشد')])


class CourseSelectForm(FlaskForm):
    professor = SelectField('استاد', validators=[InputRequired(message='استاد نمیتواند خالی باشد')])
    students = SelectMultipleField('دانشجویان')


class AboutUpdateForm(FlaskForm):
    about = TextAreaField('درباره درس')


class SyllabusForm(FlaskForm):
    title = TextAreaField('عنوان', validators=[InputRequired(message='عنوان نمیتواند خالی باشد')])


class PostForm(FlaskForm):
    title = StringField('عنوان', validators=[InputRequired(message='عنوان نمیتواند خالی باشد')])
    content = TextAreaField('متن خبر', validators=[InputRequired(message='متن خبر نمیتواند خالی باشد')])


class ResourceForm(FlaskForm):
    title = TextAreaField('عنوان', validators=[InputRequired(message='عنوان نمیتواند خالی باشد')])


class SlideForm(FlaskForm):
    title = TextAreaField('عنوان', validators=[InputRequired(message='عنوان نمیتواند خالی باشد')])
    filename = FileField('فایل', validators=[
        FileRequired(message='فایل نمی تواند خالی باشد'),
        FileAllowed(['jpg', 'png', 'pdf', 'doc', 'docx'], 'فرمت فایل معتبر نمی باشد')
    ])


class HomeworkForm(FlaskForm):
    title = StringField('عنوان', validators=[InputRequired(message='عنوان نمیتواند خالی باشد')])
    content = TextAreaField('توضیحات', validators=[InputRequired(message='عنوان نمیتواند خالی باشد')])
    deadline = DateField('مهلت ارسال', validators=[InputRequired(message='مهلت ارسال نمیتواند خالی باشد')])
    filename = FileField('فایل', validators=[
        FileRequired(message='فایل نمی تواند خالی باشد'),
        FileAllowed(['jpg', 'png', 'pdf', 'doc', 'docx'], 'فرمت فایل معتبر نمی باشد')
    ])


class HomeworkUpdateForm(FlaskForm):
    title = StringField('عنوان', validators=[InputRequired(message='عنوان نمیتواند خالی باشد')])
    content = TextAreaField('توضیحات', validators=[InputRequired(message='عنوان نمیتواند خالی باشد')])
    deadline = DateField('مهلت ارسال', validators=[InputRequired(message='مهلت ارسال نمیتواند خالی باشد')])
    filename = FileField('فایل', validators=[
        FileAllowed(['jpg', 'png', 'pdf', 'doc', 'docx'], 'فرمت فایل معتبر نمی باشد')
    ])


class SlideUpdateForm(FlaskForm):
    title = StringField('عنوان', validators=[InputRequired(message='عنوان نمیتواند خالی باشد')])
    filename = FileField('فایل', validators=[
        FileAllowed(['jpg', 'png', 'pdf', 'doc', 'docx'], 'فرمت فایل معتبر نمی باشد')
    ])


class AnswerForm(FlaskForm):
    filename = FileField('فایل', validators=[
        FileRequired(message='فایل نمی تواند خالی باشد'),
        FileAllowed(['jpg', 'png', 'pdf', 'doc', 'docx'], 'فرمت فایل معتبر نمی باشد')
    ])


class AnswerUpdateForm(FlaskForm):
    filename = FileField('فایل', validators=[
        FileRequired(message='فایل نمی تواند خالی باشد'),
        FileAllowed(['jpg', 'png', 'pdf', 'doc', 'docx'], 'فرمت فایل معتبر نمی باشد')
    ])
