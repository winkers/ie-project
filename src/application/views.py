from functools import wraps

import os
from flask import flash, render_template, request, escape, redirect, url_for, abort, current_app, send_from_directory
from flask_login import login_user, logout_user, current_user, login_required
from application import app, db, login_manager
from application.models import Category, Keyword, User, News, About, Term, Course, Syllabus, Item, Post, Resource, \
    Slide, Homework, Answer
from application.forms import TermForm, ProfessorForm, UserUpdateForm, CourseForm, CourseSelectForm, AboutUpdateForm, \
    SyllabusForm, PostForm, ResourceForm, SlideForm, HomeworkForm, HomeworkUpdateForm, SlideUpdateForm, AnswerForm, \
    AnswerUpdateForm
from werkzeug import secure_filename

import re


def roles_required(*roles):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):

            if not current_user.is_authenticated:
                return app.login_manager.unauthorized()
            user_role = current_user.role
            if (roles is not None) and (user_role not in roles):
                return abort(404)
            return fn(*args, **kwargs)

        return decorated_view

    return wrapper


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.route('/')
@app.route('/index')
@app.route('/news')
@app.route('/news/<int:page>', methods=['GET'])
@app.route('/news/page/<int:page>', methods=['GET'])
def index(page=1):
    per_page = 2
    all_news = News.query.order_by(News.create_at.desc()).paginate(page, per_page, error_out=True)
    return render_template('index.html', all_news=all_news)


@app.route('/news/show/<int:news_id>', methods=['GET'])
def news(news_id=1):
    news = News.query.get_or_404(int(escape(news_id)))
    return render_template('news.html', news=news)


@app.route('/about')
def about():
    data = About.query.first_or_404()
    return render_template('about.html', about=data)


@app.route('/news/add', methods=['GET', 'POST'])
@roles_required(User.ROLE_ADMIN)
def add_news():
    errors = {}
    for key in request.form.keys():
        errors[key] = ''

    if request.method == "POST":
        if request.form['title'] != '' and request.form['content'] != '':
            title = str(escape(request.form['title']))
            content = str(escape(request.form['content']))
            categories = keywords = list()
            category_ids = request.form.getlist('category-select')
            keyword_ids = request.form.getlist('keyword-select')

            news = News(db.session.query(User.id).first().id, title, content)

            for category_id in category_ids:
                category = db.session.query(Category).get(category_id)
                if category not in categories:
                    news.categories.append(category)
                    categories.append(category)

            for keyword_id in keyword_ids:
                keyword = db.session.query(Keyword).get(keyword_id)
                if keyword not in keywords:
                    news.keywords.append(keyword)
                    keywords.append(keyword)

            if request.form['category-input'] != '':
                category_titles = split_by_comma(escape(request.form['category-input']))
                for category_title in category_titles:
                    category_id = db.session.query(Category.id).filter_by(title=category_title).scalar()
                    if category_id is not None:
                        category = db.session.query(Category).get(category_id)
                    else:
                        category = Category(category_title)
                    if category not in categories:
                        news.categories.append(category)
                        categories.append(category)

            if request.form['keyword-input'] != '':
                keyword_titles = split_by_comma(escape(request.form['keyword-input']))
                for keyword_title in keyword_titles:
                    keyword_id = db.session.query(Keyword.id).filter_by(title=keyword_title).scalar()
                    if keyword_id is not None:
                        keyword = db.session.query(Keyword).get(keyword_id)
                    else:
                        keyword = Keyword(keyword_title)
                    if keyword not in keywords:
                        news.keywords.append(keyword)
                        keywords.append(keyword)

            db.session.add(news)
            db.session.commit()
            flash('ایجاد خبر با موفقیت انجام شد.', 'success')
        else:
            if request.form['title'] == '':
                errors['title'] = 'عنوان خبر نمیتواند خالی باشد'
            if request.form['content'] == '':
                errors['content'] = 'متن خبر نمیتواند خالی باشد'
            flash('ایجاد خبر با شکست رو به رو شد.', 'error')

    categories = db.session.query(Category).all()
    keywords = db.session.query(Keyword).all()
    return render_template('add_news.html', categories=categories, keywords=keywords, errors=errors)


def split_by_comma(string):
    return [x for x in list(set(re.split(',+', string.replace(' ', '')))) if x]


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        abort(404)
    errors = {}
    for key in request.form.keys():
        errors[key] = ''
    if request.method == "POST":
        username = str(escape(request.form['username']))
        password = str(escape(request.form['password']))
        if username and password:
            user = db.session.query(User).filter(User.username == username).first()
            if user is not None and user.check_password(password):
                login_user(user, remember=False)
                return redirect(request.args.get('next') or url_for('index'))
            else:
                flash('نام کاریری یا رمز عبور اشتباه است', 'error')
        else:
            flash('عملیات تایید هویت با مشکل مواجه شد', 'error')
            if not username:
                errors['username'] = 'نام کاربری نمیتواند خالی باشد'
            if not password:
                errors['password'] = 'رمزعبور نمیتواند خالی باشد'
    return render_template('login.html', errors=errors)


@app.route('/logout', methods=['GET'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if current_user.is_authenticated:
        abort(404)
    errors = {}
    for key in request.form.keys():
        errors[key] = ''
    if request.method == "POST":
        firstname = str(escape(request.form['firstname']))
        lastname = str(escape(request.form['lastname']))
        username = str(escape(request.form['username']))
        password = str(escape(request.form['password']))
        if firstname and lastname and username and password:
            user = db.session.query(User).filter(User.username == username).first()
            if user is None:
                user = User(firstname=firstname, lastname=lastname, username=username, password=password)
                db.session.add(user)
                db.session.commit()
                flash('عملیات ایجاد حساب کاربری با موفقیت انجام شد', 'success')
                return redirect(request.args.get('next') or url_for('login'))
            else:
                flash('عملیات ایجاد حساب کاربری با مشکل مواجه شد', 'error')
                errors['username'] = 'این نام کاربری قبلا انتخاب شده است'
        else:
            flash('عملیات ایجاد حساب کاربری با مشکل مواجه شد', 'error')
            if not firstname:
                errors['firstname'] = 'نام نمیتواند خالی باشد'
            if not lastname:
                errors['lastname'] = 'نام خانوادگی نمیتواند خالی باشد'
            if not username:
                errors['username'] = 'نام کاربری نمیتواند خالی باشد'
            if not password:
                errors['password'] = 'رمزعبور نمیتواند خالی باشد'
    return render_template('signup.html', errors=errors)


@app.route('/profile/<string:username>', methods=['GET'])
@login_required
def profile(username):
    username = str(escape(username))
    user = db.session.query(User).filter(User.username == username).first()
    if user is None:
        abort(404)
    return render_template('profile.html', user=user, User=User)


@app.errorhandler(404)
def not_found(error):
    return render_template('error_404.html', error=error), 404


@app.route('/terms/create', methods=['POST', 'GET'])
@roles_required(User.ROLE_ADMIN)
def term_create():
    form = TermForm()
    if form.is_submitted():
        if form.validate():
            term = Term(semester=form.semester.data, year=form.year.data)
            db.session.add(term)
            db.session.commit()
            flash('عملیات ایجاد ترم با موفقیت انجام شد', 'success')
        else:
            flash('ایجاد ترم با شکست رو به رو شد.', 'error')
    return render_template('term/create.html', form=form)


@app.route('/professors/create', methods=['POST', 'GET'])
@roles_required(User.ROLE_ADMIN)
def professor_create():
    form = ProfessorForm()
    if form.is_submitted():
        if form.validate():
            professor = User(form.firstname.data, form.lastname.data, form.username.data, form.password.data,
                             role=User.ROLE_PROFESSOR)
            db.session.add(professor)
            db.session.commit()
            flash('عملیات ایجاد استاد با موفقیت انجام شد', 'success')
            return redirect(url_for('professor_create'))
        else:
            flash('ایجاد استاد با شکست رو به رو شد.', 'error')
    return render_template('professor/create.html', form=form)


@app.route('/professors')
@app.route('/professors/index')
@roles_required(User.ROLE_ADMIN)
def professor_list():
    professors = User.query.filter(User.role == User.ROLE_PROFESSOR).all()
    return render_template('professor/index.html', professors=professors)


@app.route('/professors/update/<int:professor_id>', methods=['POST', 'GET'])
@roles_required(User.ROLE_ADMIN)
def professor_update(professor_id):
    professor = User.query.get_or_404(professor_id)
    if not professor.is_professor():
        abort(404)

    form = UserUpdateForm()

    if form.is_submitted():
        if form.validate():
            professor.set_password(form.password.data)
            db.session.commit()
            flash('عملیات ویرایش استاد با موفقیت انجام شد', 'success')
            return redirect(url_for('professor_list'))
        else:
            flash('ویرایش استاد با شکست رو به رو شد.', 'error')

    return render_template('professor/update.html', form=form)


@app.route('/students')
@app.route('/students/index')
@roles_required(User.ROLE_ADMIN)
def student_list():
    students = User.query.filter(User.role == User.ROLE_STUDENT).all()
    if not students:
        abort(404)
    return render_template('student/index.html', students=students)


@app.route('/students/update/<int:student_id>', methods=['POST', 'GET'])
@roles_required(User.ROLE_ADMIN)
def student_update(student_id):
    student = User.query.get_or_404(student_id)
    if not student.is_student():
        abort(404)

    form = UserUpdateForm()

    if form.is_submitted():
        if form.validate():
            student.set_password(form.password.data)
            db.session.commit()
            flash('عملیات ویرایش دانشجو با موفقیت انجام شد', 'success')
            return redirect(url_for('student_list'))
        else:
            flash('ویرایش دانشجو با شکست رو به رو شد.', 'error')

    return render_template('student/update.html', form=form)


@app.route('/students/verify/<int:student_id>')
@roles_required(User.ROLE_ADMIN)
def student_verify(student_id):
    student = User.query.get_or_404(student_id)
    if (not student.is_student()) or (student.is_verified()):
        abort(404)

    student.status = User.STATUS_VERIFIED
    db.session.commit()
    flash('عملیات تایید هویت دانشجو با موفقیت انجام شد', 'success')

    return redirect(url_for('student_list'))


@app.route('/courses/show/<int:course_id>', methods=['GET'])
@login_required
@roles_required(User.ROLE_PROFESSOR, User.ROLE_STUDENT)
def course(course_id):
    course = db.session.query(Course).get(int(escape(course_id)))

    if course is None:
        abort(404)
    if course.has_access(current_user):
        return render_template('course.html', course=course)
    else:
        print('s')
        abort(404)


@app.route('/courses/create', methods=['POST', 'GET'])
@roles_required(User.ROLE_ADMIN)
def course_create():
    form = CourseForm()
    form.term.choices = [(str(a.id), a) for a in Term.query]

    if form.is_submitted():
        if form.validate():
            course = Course(title=form.title.data, term_id=form.term.data)
            db.session.add(course)
            db.session.commit()
            flash('عملیات ایجاد درس با موفقیت انجام شد', 'success')
            return redirect(url_for('course_list'))
        else:
            flash('ایجاد درس با شکست رو به رو شد.', 'error')
    return render_template('course/create.html', form=form)


@app.route('/courses')
def course_list():
    terms = db.session.query(Term).all()
    return render_template('course/index.html', terms=terms)


@app.route('/courses/close/<int:course_id>')
@roles_required(User.ROLE_ADMIN)
def course_close(course_id):
    course = Course.query.get_or_404(course_id)

    if course.is_close():
        abort(404)

    course.status = Course.STATUS_CLOSE
    db.session.commit()
    flash('عملیات بستن درس با موفقیت انجام شد', 'success')

    return redirect(url_for('student_list'))


@app.route('/courses/select/<int:course_id>', methods=['POST', 'GET'])
@roles_required(User.ROLE_ADMIN)
def course_select(course_id):
    course = Course.query.get_or_404(course_id)

    form = CourseSelectForm()

    professors = User.query.filter(User.role == User.ROLE_PROFESSOR)
    students = User.query.filter(User.role == User.ROLE_STUDENT)
    form.professor.choices = [(str(professor.id), professor) for professor in professors]
    form.students.choices = [(str(student.id), student) for student in students]

    if form.is_submitted():
        if form.validate():

            for user in course.users:
                course.users.remove(user)
            db.session.commit()

            professor = User.query.get(int(form.professor.data))
            course.users.append(professor)
            db.session.commit()

            for student in students:
                if str(student.id) in form.students.data:
                    course.users.append(student)
            db.session.commit()

            flash('عملیات انتخاب اعضای درس با موفقیت انجام شد', 'success')
            return redirect(url_for('course_list'))

        flash('انتخاب اعضای درس با شکست رو به رو شد.', 'error')

    form.professor.data = []
    form.students.data = []
    for x in course.users:
        if x.is_professor():
            form.professor.data.append(str(x.id))
        else:
            form.students.data.append(str(x.id))

    return render_template('course/select.html', form=form)


@app.route('/courses/update/<int:course_id>')
@roles_required(User.ROLE_PROFESSOR)
def course_update(course_id):
    course = Course.query.get_or_404(course_id)

    if course.is_close():
        abort(404)

    form = AboutUpdateForm()
    form.about.data = course.about

    syllabus = course.syllabus

    return render_template('course/update.html', course=course, form=form)


@app.route('/courses/about/update/<int:course_id>', methods=['POST'])
@roles_required(User.ROLE_PROFESSOR)
def course_about_update(course_id):
    course = Course.query.get_or_404(course_id)
    form = AboutUpdateForm()

    if form.validate_on_submit():
        course.about = form.about.data
        db.session.commit()
        flash('عملیات ویرایش درباره درس با موفقیت انجام شد', 'success')
        return redirect(url_for('course_update', course_id=course_id))
    return redirect(url_for('course_update', course_id=course.id))


@app.route('/courses/syllabus/create/<int:course_id>', methods=['POST', 'GET'])
@roles_required(User.ROLE_PROFESSOR)
def course_syllabus_item_create(course_id):
    course = Course.query.get_or_404(course_id)

    form = SyllabusForm()

    if form.is_submitted():
        if form.validate():
            syllabus = course.syllabus
            if not syllabus:
                syllabus = Syllabus(course_id)
                db.session.add(syllabus)
                db.session.commit()
            item = Item(syllabus.id, form.title.data)
            db.session.add(item)
            db.session.commit()
            flash('عملیات ساخت آیتم سیلابس با موفقیت انجام شد', 'success')
            return redirect(url_for('course_update', course_id=course_id))
        flash('عملیات ساخت آیتم سیلابس با شکست رو به رو شد.', 'error')
    return render_template('course/syllabus_item_create.html', form=form)


@app.route('/courses/syllabus/update/<int:item_id>', methods=['POST', 'GET'])
@roles_required(User.ROLE_PROFESSOR)
def course_syllabus_item_update(item_id):
    item = Item.query.get_or_404(item_id)

    form = SyllabusForm()

    if form.is_submitted():
        if form.validate():
            item.title = form.title.data
            db.session.commit()
            flash('عملیات ویرایش آیتم با موفقیت انجام شد', 'success')
            return redirect(url_for('course_update', course_id=item.syllabus.course_id))
        else:
            flash('ویرایش آیتم با شکست رو به رو شد.', 'error')

    form.title.data = item.title
    return render_template('course/syllabus_item_update.html', form=form)


@app.route('/courses/syllabus/delete/<int:item_id>', methods=['POST', 'GET'])
@roles_required(User.ROLE_PROFESSOR)
def course_syllabus_item_delete(item_id):
    item = Item.query.get_or_404(item_id)
    course_id = item.syllabus.course_id
    # if current_user != item.syllabus.course.get_professor():
    #     abort(404)

    db.session.delete(item)
    db.session.commit()

    flash('عملیات حذف آیتم با موفقیت انجام شد', 'success')
    return redirect(url_for('course_update', course_id=course_id))


@app.route('/courses/post/create/<int:course_id>', methods=['POST', 'GET'])
@roles_required(User.ROLE_PROFESSOR)
def course_post_create(course_id):
    course = Course.query.get_or_404(course_id)

    form = PostForm()

    if form.is_submitted():
        if form.validate():
            post = Post(course_id=course.id, title=form.title.data, content=form.content.data)
            db.session.add(post)
            db.session.commit()
            flash('عملیات ایجاد خبر با موفقیت انجام شد', 'success')
            return redirect(url_for('course_update', course_id=post.course_id))
        else:
            flash('ایجاد خبر با شکست رو به رو شد.', 'error')
    return render_template('course/post_create.html', form=form)


@app.route('/courses/post/update/<int:post_id>', methods=['POST', 'GET'])
@roles_required(User.ROLE_PROFESSOR)
def course_post_update(post_id):
    post = Post.query.get_or_404(post_id)

    form = PostForm()

    if form.is_submitted():
        if form.validate():
            post.title = form.title.data
            post.content = form.content.data
            db.session.commit()
            flash('عملیات ویرایش خبر با موفقیت انجام شد', 'success')
            return redirect(url_for('course_update', course_id=post.course_id))
        else:
            flash('ویرایش خبر با شکست رو به رو شد.', 'error')

    form.title.data = post.title
    form.content.data = post.content
    return render_template('course/post_update.html', form=form)


@app.route('/courses/post/delete/<int:post_id>', methods=['POST', 'GET'])
@roles_required(User.ROLE_PROFESSOR)
def course_post_delete(post_id):
    post = Post.query.get_or_404(post_id)
    course_id = post.course_id

    db.session.delete(post)
    db.session.commit()
    flash('عملیات حذف خبر با موفقیت انجام شد', 'success')
    return redirect(url_for('course_update', course_id=course_id))


@app.route('/courses/resource/create/<int:course_id>', methods=['POST', 'GET'])
@roles_required(User.ROLE_PROFESSOR)
def course_resource_create(course_id):
    course = Course.query.get_or_404(course_id)

    form = ResourceForm()

    if form.is_submitted():
        if form.validate():
            resource = Resource(course_id=course.id, title=form.title.data)
            db.session.add(resource)
            db.session.commit()
            flash('عملیات ایجاد منبع با موفقیت انجام شد', 'success')
            return redirect(url_for('course_update', course_id=course.id))
        else:
            flash('ایجاد منبع با شکست رو به رو شد.', 'error')
    return render_template('course/resource_create.html', form=form)


@app.route('/courses/resource/update/<int:resource_id>', methods=['POST', 'GET'])
@roles_required(User.ROLE_PROFESSOR)
def course_resource_update(resource_id):
    resource = Resource.query.get_or_404(resource_id)

    form = ResourceForm()

    if form.is_submitted():
        if form.validate():
            resource.title = form.title.data
            db.session.commit()
            flash('عملیات ویرایش منبع با موفقیت انجام شد', 'success')
            return redirect(url_for('course_update', course_id=resource.course_id))
        else:
            flash('ویرایش منبع با شکست رو به رو شد.', 'error')

    form.title.data = resource.title
    return render_template('course/resource_update.html', form=form)


@app.route('/courses/resource/delete/<int:resource_id>', methods=['POST', 'GET'])
@roles_required(User.ROLE_PROFESSOR)
def course_resource_delete(resource_id):
    resource = Resource.query.get_or_404(resource_id)
    course_id = resource.course_id

    db.session.delete(resource)
    db.session.commit()
    flash('عملیات حذف منبع با موفقیت انجام شد', 'success')
    return redirect(url_for('course_update', course_id=course_id))


@app.route('/courses/slide/create/<int:course_id>', methods=['GET', 'POST'])
@roles_required(User.ROLE_PROFESSOR)
def course_slide_create(course_id):
    course = Course.query.get_or_404(course_id)

    form = SlideForm()

    if form.is_submitted():
        file = form.filename.data
        if form.validate():
            filename = secure_filename(file.filename)

            slide = Slide(course_id=course.id, title=form.title.data, filename=filename)
            db.session.add(slide)
            db.session.commit()

            filename = "course" + str(course.id) + "_slide" + str(slide.id) + "_" + filename
            file.save(os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'], filename))
            flash('عملیات ایجاد اسلاید با موفقیت انجام شد', 'success')
            return redirect(url_for('course_update', course_id=course.id))

        flash('ایجاد اسلاید با شکست رو به رو شد.', 'error')

    return render_template('course/slide_create.html', form=form)


@app.route('/courses/slide/update/<int:slide_id>', methods=['GET', 'POST'])
@roles_required(User.ROLE_PROFESSOR)
def course_slide_update(slide_id):
    slide = Slide.query.get_or_404(slide_id)

    form = SlideUpdateForm()

    if form.is_submitted():
        if form.validate():
            slide.title = form.title.data
            db.session.commit()

            file = form.filename.data
            if file:
                filename = secure_filename(file.filename)
                pre_filename = slide.filename

                slide.filename = filename
                db.session.commit()

                filename = "course" + str(slide.course.id) + "_slide" + str(slide.id) + "_" + filename
                pre_filename = "course" + str(slide.course.id) + "_slide" + str(slide.id) + "_" + pre_filename

                os.remove(os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'], pre_filename))
                file.save(os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'], filename))

            flash('عملیات ویرایش اسلاید با موفقیت انجام شد', 'success')
            return redirect(url_for('course_update', course_id=slide.course.id))

        flash('ویرایش اسلاید با شکست رو به رو شد.', 'error')

    form.title.data = slide.title
    return render_template('course/slide_update.html', form=form)


@app.route('/courses/slide/delete/<int:slide_id>', methods=['GET', 'POST'])
@roles_required(User.ROLE_PROFESSOR)
def course_slide_delete(slide_id):
    slide = Slide.query.get_or_404(slide_id)
    course_id = slide.course_id

    filename = "course" + str(slide.course.id) + "_slide" + str(slide.id) + "_" + slide.filename
    os.remove(os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'], filename))

    db.session.delete(slide)
    db.session.commit()

    flash('عملیات حذف اسلاید با موفقیت انجام شد', 'success')
    return redirect(url_for('course_update', course_id=course_id))


@app.route('/courses/slide/view/<int:slide_id>', methods=['GET', 'POST'])
@roles_required(User.ROLE_PROFESSOR, User.ROLE_STUDENT)
def course_slide_view(slide_id):
    slide = Slide.query.get_or_404(slide_id)
    filename = "course" + str(slide.course.id) + "_slide" + str(slide.id) + "_" + slide.filename
    uploads = os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'])
    return send_from_directory(directory=uploads, filename=filename)


@app.route('/courses/homework/create/<int:course_id>', methods=['GET', 'POST'])
@roles_required(User.ROLE_PROFESSOR)
def course_homework_create(course_id):
    course = Course.query.get_or_404(course_id)

    form = HomeworkForm()

    if form.is_submitted():
        if form.validate():
            title = form.title.data
            content = form.content.data
            deadline_at = form.deadline.data
            file = form.filename.data
            filename = secure_filename(file.filename)

            homework = Homework(course_id=course.id, title=title, filename=filename, content=content,
                                deadline_at=deadline_at)
            db.session.add(homework)
            db.session.commit()

            filename = "course" + str(course.id) + "_homework" + str(homework.id) + "_" + filename
            file.save(os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'], filename))
            flash('عملیات ایجاد تمرین با موفقیت انجام شد', 'success')
            return redirect(url_for('course_update', course_id=course.id))

        flash('ایجاد تمرین با شکست رو به رو شد.', 'error')

    return render_template('course/homework_create.html', form=form)


@app.route('/courses/homework/update/<int:homework_id>', methods=['POST'])
@roles_required(User.ROLE_PROFESSOR)
def course_homework_update(homework_id):
    homework = Homework.query.get_or_404(homework_id)
    form = HomeworkUpdateForm()

    if form.validate_on_submit():
        homework.title = form.title.data
        homework.content = form.content.data
        homework.deadline_at = form.deadline.data
        db.session.commit()

        file = form.filename.data
        if file:
            filename = secure_filename(file.filename)
            pre_filename = homework.filename

            homework.filename = filename
            db.session.commit()

            filename = "course" + str(homework.course.id) + "_homework" + str(homework.id) + "_" + filename
            pre_filename = "course" + str(homework.course.id) + "_homework" + str(homework.id) + "_" + pre_filename

            os.remove(os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'], pre_filename))
            file.save(os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'], filename))
        flash('عملیات ویرایش تمرین با موفقیت انجام شد', 'success')
    else:
        flash('ویرایش تمرین با شکست رو به رو شد.', 'error')

    return render_template('course/homework_index.html', form=form, homework=homework)


@app.route('/courses/homework/<int:homework_id>', methods=['GET', 'POST'])
@roles_required(User.ROLE_PROFESSOR)
def course_homework_index(homework_id):
    homework = Homework.query.get_or_404(homework_id)

    form = HomeworkUpdateForm()
    form.title.data = homework.title
    form.content.data = homework.content
    form.deadline.data = homework.deadline_at

    return render_template('course/homework_index.html', form=form, homework=homework)


@app.route('/courses/homework/delete/<int:homework_id>', methods=['GET', 'POST'])
@roles_required(User.ROLE_PROFESSOR)
def course_homework_delete(homework_id):
    homework = Homework.query.get_or_404(homework_id)
    course_id = homework.course_id

    filename = "course" + str(homework.course.id) + "_homework" + str(homework.id) + "_" + homework.filename
    os.remove(os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'], filename))

    db.session.delete(homework)
    db.session.commit()

    flash('عملیات حذف تمرین با موفقیت انجام شد', 'success')
    return redirect(url_for('course_update', course_id=course_id))


@app.route('/courses/homework/answer/<int:course_id>', methods=['GET', 'POST'])
@roles_required(User.ROLE_STUDENT)
def course_homework_answer_index(course_id):
    course = Course.query.get_or_404(course_id)

    if (course is not None) and current_user in course.get_students() or current_user is course.get_professor():
        if not course.has_homeworks():
            abort(404)
        homeworks = course.homeworks
        return render_template('course/homework_answer_index.html', homeworks=homeworks)
    abort(404)


@app.route('/courses/homework/answer/create/<int:homework_id>', methods=['GET', 'POST'])
@roles_required(User.ROLE_STUDENT)
def course_homework_answer_create(homework_id):
    homework = Homework.query.get_or_404(homework_id)
    course = homework.course

    answer = Answer.query.filter(Answer.homework_id == homework_id, Answer.user_id == current_user.id).first()
    if answer:
        abort(404)

    form = AnswerForm()

    if form.is_submitted():
        if form.validate():
            file = form.filename.data
            filename = secure_filename(file.filename)
            answer = Answer(homework_id=homework_id, user_id=current_user.id, filename=filename)
            db.session.add(answer)
            db.session.commit()

            filename = "course" + str(answer.homework.course_id) + "_homework" + str(homework_id) + "_answer" + str(
                answer.id) + "_" + filename
            file.save(os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'], filename))
            flash('عملیات آپلود پاسخ تمرین با موفقیت انجام شد', 'success')
            return redirect(url_for('course_homework_answer_index', course_id=answer.homework.course_id))

        flash('آپلود پاسخ تمرین با شکست رو به رو شد.', 'error')

    return render_template('course/homework_answer_create.html', form=form)


@app.route('/courses/homework/answer/update/<int:homework_id>', methods=['GET', 'POST'])
@roles_required(User.ROLE_STUDENT)
def course_homework_answer_update(homework_id):
    homework = Homework.query.get_or_404(homework_id)
    answer = Answer.query.filter(Answer.homework_id == homework_id, Answer.user_id == current_user.id).first_or_404()

    form = AnswerUpdateForm()

    if form.is_submitted():
        if form.validate():
            file = form.filename.data

            filename = secure_filename(file.filename)
            pre_filename = answer.filename

            answer.filename = filename
            db.session.commit()

            filename = "course" + str(homework.course.id) + "_homework" + str(homework_id) + "_answer" + str(
                answer.id) + "_" + filename
            pre_filename = "course" + str(homework.course.id) + "_homework" + str(homework_id) + "_answer" + str(
                answer.id) + "_" + pre_filename

            os.remove(os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'], pre_filename))
            file.save(os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'], filename))

            flash('عملیات ویرایش پاسخ تمرین با موفقیت انجام شد', 'success')
            return redirect(url_for('course_homework_answer_index', course_id=homework.course_id))

        flash('یرایش پاسخ تمرین با شکست رو به رو شد.', 'error')

    return render_template('course/homework_answer_update.html', form=form)


@app.route('/courses/homework/answer/delete/<int:homework_id>', methods=['GET', 'POST'])
@roles_required(User.ROLE_STUDENT)
def course_homework_answer_delete(homework_id):
    homework = Homework.query.get_or_404(homework_id)
    answer = Answer.query.filter(Answer.homework_id == homework_id, Answer.user_id == current_user.id).first_or_404()

    filename = "course" + str(homework.course.id) + "_homework" + str(homework.id) + "_answer" + str(
        answer.id) + "_" + answer.filename
    os.remove(os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'], filename))

    db.session.delete(answer)
    db.session.commit()

    flash('عملیات حذف پاسخ تمرین با موفقیت انجام شد', 'success')
    return redirect(url_for('course_homework_answer_index', course_id=homework.course_id))


@app.route('/courses/homework/answer/view/<int:homework_id>/<int:user_id>', methods=['GET', 'POST'])
@roles_required(User.ROLE_STUDENT, User.ROLE_PROFESSOR)
def course_homework_answer_view(homework_id, user_id):
    homework = Homework.query.get_or_404(homework_id)
    professor = homework.course.get_professor()

    if (user_id == current_user.id) or (professor and professor.id == current_user.id):
        answer = Answer.query.filter(Answer.homework_id == homework_id, Answer.user_id == user_id).first_or_404()
        filename = "course" + str(homework.course.id) + "_homework" + str(homework.id) + "_answer" + str(
            answer.id) + "_" + answer.filename
        uploads = os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'])
        return send_from_directory(directory=uploads, filename=filename)
    else:
        abort(404)


if __name__ == '__main__':
    app.run()
