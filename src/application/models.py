from application import db
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
import datetime

user_course = db.Table('user_course',
                       db.Column('user_id', db.Integer, db.ForeignKey('user.id'), primary_key=True),
                       db.Column('course_id', db.Integer, db.ForeignKey('course.id'), primary_key=True))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(40))
    lastname = db.Column(db.String(40))
    username = db.Column(db.String(40), nullable=False, unique=True)
    password_hash = db.Column(db.String(100), nullable=False)
    status = db.Column(db.SmallInteger, nullable=False, default=0)
    role = db.Column(db.SmallInteger, nullable=False, default=0)
    create_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    update_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    news = db.relationship('News', backref='user', lazy='dynamic')
    contacts = db.relationship('Contact', backref='user', lazy='dynamic')

    STATUS_WAITING = 0
    STATUS_VERIFIED = 1

    ROLE_STUDENT = 0
    ROLE_PROFESSOR = 1
    ROLE_ADMIN = 2

    def __init__(self, firstname, lastname, username, password, role=ROLE_STUDENT):
        self.firstname = firstname
        self.lastname = lastname
        self.username = username.lower()
        self.set_password(password)
        self.role = role
        if self.role != self.ROLE_STUDENT:
            self.status = self.STATUS_VERIFIED

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def is_student(self):
        return self.role == self.ROLE_STUDENT

    def is_professor(self):
        return self.role == self.ROLE_PROFESSOR

    def is_admin(self):
        return self.role == self.ROLE_ADMIN

    def is_verified(self):
        return self.status == self.STATUS_VERIFIED

    def __repr__(self):
        return self.firstname + ' ' + self.lastname


class Term(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    semester = db.Column(db.SmallInteger, default=0)
    year = db.Column(db.Integer)

    courses = db.relationship('Course', backref='term', lazy='dynamic')

    SEMESTER_ONE = 0
    SEMESTER_TWO = 1
    SEMESTER_SUMMER = 2

    def __init__(self, year, semester=None):
        self.year = year
        self.semester = semester

    @staticmethod
    def get_semester_list():
        return (str(Term.SEMESTER_ONE), 'نیمسال اول'), (str(Term.SEMESTER_TWO), 'نیمسال دوم'), (
            str(Term.SEMESTER_SUMMER), 'ترم تابستان')

    def get_semester_label(self):
        for value, label in self.get_semester_list():
            if value == str(self.semester):
                return label

    def __repr__(self):
        return self.get_semester_label() + ' ' + str(self.year)


class Course(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    term_id = db.Column(db.Integer, db.ForeignKey('term.id'), nullable=False)
    title = db.Column(db.String(120), nullable=False)
    status = db.Column(db.SmallInteger, nullable=False, default=0)
    about = db.Column(db.Text)

    users = db.relationship('User', secondary=user_course, backref='courses', lazy='dynamic')

    syllabus = db.relationship("Syllabus", uselist=False, backref="course")
    resources = db.relationship('Resource', backref='course', lazy='dynamic')
    slides = db.relationship('Slide', backref='course', lazy='dynamic')
    homeworks = db.relationship('Homework', backref='course', lazy='dynamic')
    posts = db.relationship('Post', backref='course', lazy='dynamic')

    STATUS_OPEN = 0
    STATUS_CLOSE = 1

    def __init__(self, title, term_id, status=None):
        self.title = title
        self.term_id = term_id
        self.status = status

    def get_professor(self):
        for user in self.users:
            if user.role == User.ROLE_PROFESSOR:
                return user
        return None

    def get_students(self):
        user_list = []
        for user in self.users:
            if user.role == User.ROLE_STUDENT:
                user_list.append(user)
        return user_list

    def has_professor(self):
        return self.get_professor() is not None

    def has_access(self, user):
        return True if user in self.users else False

    def has_posts(self):
        return db.session.query(db.exists().where(Post.course_id == self.id)).scalar()

    def has_syllabus(self):
        return db.session.query(db.exists().where(Syllabus.course_id == self.id)).scalar()

    def has_resources(self):
        return db.session.query(db.exists().where(Resource.course_id == self.id)).scalar()

    def has_slides(self):
        return db.session.query(db.exists().where(Slide.course_id == self.id)).scalar()

    def has_homeworks(self):
        return db.session.query(db.exists().where(Homework.course_id == self.id)).scalar()

    def is_open(self):
        return self.status == self.STATUS_OPEN

    def is_close(self):
        return self.status == self.STATUS_CLOSE


class Syllabus(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    course_id = db.Column(db.Integer, db.ForeignKey('course.id'), nullable=False, unique=True)
    items = db.relationship('Item', backref='syllabus', lazy='dynamic')
    create_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    update_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def has_items(self):
        return Item.query.filter_by(syllabus_id=self.id).scalar() is not None

    def __init__(self, course_id):
        self.course_id = course_id


class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    syllabus_id = db.Column(db.Integer, db.ForeignKey('syllabus.id'), nullable=False)
    title = db.Column(db.String(120))

    def __init__(self, syllabus_id, title):
        self.syllabus_id = syllabus_id
        self.title = title


class Resource(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    course_id = db.Column(db.Integer, db.ForeignKey('course.id'), nullable=False)
    title = db.Column(db.String(120))

    def __init__(self, course_id, title):
        self.course_id = course_id
        self.title = title


class Slide(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    course_id = db.Column(db.Integer, db.ForeignKey('course.id'), nullable=False)
    filename = db.Column(db.String(120))
    title = db.Column(db.String(120))
    create_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    update_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def __init__(self, course_id, title, filename):
        self.course_id = course_id
        self.title = title
        self.filename = filename


class Homework(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    course_id = db.Column(db.Integer, db.ForeignKey('course.id'), nullable=False)
    title = db.Column(db.String(120))
    content = db.Column(db.Text)
    filename = db.Column(db.Text)
    deadline_at = db.Column(db.Date)
    create_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    update_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    answers = db.relationship('Answer', backref='homework', lazy='dynamic')

    def user_has_answer(self, user):
        return db.session.query(
            db.exists().where(Answer.homework_id == self.id).where(Answer.user_id == user.id)).scalar()

    def __init__(self, course_id, title, content, filename, deadline_at):
        self.course_id = course_id
        self.title = title
        self.content = content
        self.filename = filename
        self.deadline_at = deadline_at


class Answer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    homework_id = db.Column(db.Integer, db.ForeignKey('homework.id'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    filename = db.Column(db.Text)

    user = db.relationship('User')

    create_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    update_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def __init__(self, homework_id, user_id, filename):
        self.homework_id = homework_id
        self.user_id = user_id
        self.filename = filename


class Contact(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    website_id = db.Column(db.Integer, db.ForeignKey('website.id'), nullable=False)
    url = db.Column(db.String(120))

    def __init__(self, user_id, website_id, url):
        self.user_id = user_id
        self.website_id = website_id
        self.url = url


class Website(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120), unique=True)

    contacts = db.relationship('Contact', backref='website', lazy='dynamic')

    def __init__(self, title):
        self.title = title


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120), unique=True)

    def __init__(self, title):
        self.title = title


class Keyword(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120), unique=True)

    def __init__(self, title):
        self.title = title


news_category = db.Table('news_category', db.Column('news_id', db.Integer, db.ForeignKey('news.id'), primary_key=True),
                         db.Column('category_id', db.Integer, db.ForeignKey('category.id'), primary_key=True))

news_keyword = db.Table('news_keyword', db.Column('news_id', db.Integer, db.ForeignKey('news.id'), primary_key=True),
                        db.Column('keyword_id', db.Integer, db.ForeignKey('keyword.id'), primary_key=True))


class News(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    title = db.Column(db.String(120))
    content = db.Column(db.Text)
    create_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    update_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    categories = db.relationship('Category', secondary=news_category, backref='news', lazy='dynamic')
    keywords = db.relationship('Keyword', secondary=news_keyword, backref='news', lazy='dynamic')

    def __init__(self, user_id, title, content):
        self.user_id = user_id
        self.title = title
        self.content = content


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    course_id = db.Column(db.Integer, db.ForeignKey('course.id'), nullable=False)
    title = db.Column(db.String(120))
    content = db.Column(db.Text)
    create_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    update_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def __init__(self, course_id, title, content):
        self.course_id = course_id
        self.title = title
        self.content = content


class About(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.Text)
    create_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    update_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def __init__(self, content):
        self.content = content
